﻿using System;
using System.Collections.Generic;

namespace Floyds_triangle
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var treug = new List<List<int>>();

            var rigthInput = false;
            int rowsCount = default;

            while (rigthInput == false)
            {
                Console.Write("Количество рядов: ");

                if (int.TryParse(Console.ReadLine(), out rowsCount))
                    rigthInput = true;
                else Console.WriteLine("Неверный ввод");
            }

            var counter = 1;

            for (int i = 0; i < rowsCount; i++)
            {
                var row = new List<int>(i + 1);

                for (int j = 0; j < row.Capacity; j++)
                    row.Add(counter++);

                treug.Add(row);
            }

            treug.ForEach(row =>
            {
                row.ForEach(item => Console.Write("{0} ", item));
                Console.WriteLine();
            });

            Console.ReadKey();
        }
    }
}